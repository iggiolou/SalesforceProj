package com.upwork.salesforce.ApiGet;

import com.upwork.salesforce.ApiGet.service.SOQLQueryService;
import javax.inject.Inject;
import org.json.simple.parser.ParseException;

/**
 * @author viktoriyadoroshenko
 * @since 7/6/18
 */
public class SOQLServiceTest {

  @Inject
  SOQLQueryService soqlQueryService;

  public void testRunSimpleQuery() throws ParseException {
    String query = "SELECT+name+from+Account";
    soqlQueryService.runSelectQuery(query);

  }

}
