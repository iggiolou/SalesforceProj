package com.upwork.salesforce.ApiGet;

import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.response.Response;
import net.minidev.json.JSONObject;

/**
 * @author viktoriyadoroshenko
 * @since 7/6/18
 */
public class LoginTest {

  private String PASS = "@WSXzaq1";
  private String securityToken;
  private String instanceUrl;
  private String id;

  private String USERNAME = "irish.drug@gmail.com";

  private String LOGINURL = "https://login.salesforce.com";
  private String GRANTSERVICE = "/services/oauth2/token?grant_type=password";
  private String CLIENTID = "3MVG9oNqAtcJCF.GiiH3DeX0r_5m3amcZ_wH79Oei7eSgbsWMAZfcolSHRPFgd2XUCOvMx7CUohPPftrRseh9";
  private String CLIENTSECRET = "122566590613109871";

  private String url =
      LOGINURL + GRANTSERVICE + "&client_id=" + CLIENTID + "&client_secret=" + CLIENTSECRET
          + "&username=" + USERNAME + "&password=" + PASS;

 // @Test
  public void test() {
    Response response = given()
        .post(url);
    response.getBody().prettyPrint();
    JSONObject json = response.getBody().as(JSONObject.class);
    securityToken = json.get("token_type").toString() + " " + json.get("access_token").toString();
    instanceUrl = json.get("instance_url").toString();
    id = json.get("id").toString();

  }
}
