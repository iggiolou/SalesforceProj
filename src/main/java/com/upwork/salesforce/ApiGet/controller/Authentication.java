package com.upwork.salesforce.ApiGet.controller;

import static com.upwork.salesforce.ApiGet.config.Constants.ID;
import static com.upwork.salesforce.ApiGet.config.Constants.INSTANCE_URL;
import static org.springframework.security.oauth2.common.OAuth2AccessToken.ACCESS_TOKEN;
import static org.springframework.security.oauth2.common.OAuth2AccessToken.TOKEN_TYPE;
import static sun.security.x509.X509CertImpl.SIGNATURE;

import com.upwork.salesforce.ApiGet.config.Constants;
import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

/**
 * @author viktoriyadoroshenko
 * @since 7/5/18
 */
@Controller
public class Authentication {

  RestTemplate restTemplate = new RestTemplate();

  public Map getAuthParams() throws ParseException {

    String url = Constants.url;
    Map<String, String> cookies = new HashMap<>();
    ResponseEntity<String> response = restTemplate
        .exchange(url, HttpMethod.POST, HttpEntity.EMPTY, String.class);

    JSONParser parser = new JSONParser();
    JSONObject json = (JSONObject) parser.parse(response.getBody());

    cookies.put(ACCESS_TOKEN, json.get(ACCESS_TOKEN).toString());
    cookies.put(INSTANCE_URL, json.get(INSTANCE_URL).toString());
    cookies.put(ID, json.get(ID).toString());
    cookies.put(TOKEN_TYPE, json.get(TOKEN_TYPE).toString());
    cookies.put(SIGNATURE, json.get(SIGNATURE).toString());
    System.out.println(cookies);

    return cookies;

  }

}
