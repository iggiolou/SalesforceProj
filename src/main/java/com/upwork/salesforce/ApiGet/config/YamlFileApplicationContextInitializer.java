package com.upwork.salesforce.ApiGet.config;

import java.io.IOException;
import java.util.List;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.Resource;

@Configuration
public class YamlFileApplicationContextInitializer implements
    ApplicationContextInitializer<ConfigurableApplicationContext> {

  @Override
  public void initialize(ConfigurableApplicationContext applicationContext) {
    try {
      Resource resource = applicationContext.getResource("classpath:application.yml");
      YamlPropertySourceLoader sourceLoader = new YamlPropertySourceLoader();
      List<PropertySource<?>> yamlTestProperties = sourceLoader
          .load("yamlTestProperties", resource);
      applicationContext.getEnvironment().getPropertySources()
          .addFirst(yamlTestProperties.stream().findFirst().orElseThrow(IOException::new));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
