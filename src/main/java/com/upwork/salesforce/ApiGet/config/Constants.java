package com.upwork.salesforce.ApiGet.config;

import org.springframework.context.annotation.Configuration;

/**
 * @author viktoriyadoroshenko
 * @since 7/6/18
 */
@Configuration
public class Constants {

  //TODO: migrate data to application.yml

  private static final String PASS = "@WSXzaq1";
  private static final String USERNAME = "irish.drug@gmail.com";
  private static final String LOGINURL = "https://login.salesforce.com";
  private static final String GRANTSERVICE = "/services/oauth2/token?grant_type=password";
  private static final String CLIENTID = "3MVG9oNqAtcJCF.GiiH3DeX0r_5m3amcZ_wH79Oei7eSgbsWMAZfcolSHRPFgd2XUCOvMx7CUohPPftrRseh9";
  private static final String CLIENTSECRET = "122566590613109871";


  public static final String INSTANCE_URL = "instance_url";
  public static final String ID = "id";
  public static final String URL_QUERY = "/services/data/v20.0/query?q=";


  public static String url =
      LOGINURL + GRANTSERVICE + "&client_id=" + CLIENTID + "&client_secret=" + CLIENTSECRET
          + "&username=" + USERNAME + "&password=" + PASS;

}
