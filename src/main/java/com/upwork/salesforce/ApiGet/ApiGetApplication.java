package com.upwork.salesforce.ApiGet;

import com.upwork.salesforce.ApiGet.config.YamlFileApplicationContextInitializer;
import com.upwork.salesforce.ApiGet.service.SOQLQueryService;
import org.json.simple.parser.ParseException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication(scanBasePackages = {"com.upwork.salesforce.ApiGet"})
@EnableAutoConfiguration
@Import(YamlFileApplicationContextInitializer.class)
public class ApiGetApplication {

  public static void main(String[] args) throws ParseException {
    SpringApplication.run(ApiGetApplication.class, args);
    String query =  "SELECT+name+from+Account";
    new SOQLQueryService().runSelectQuery(query);
  }

}
