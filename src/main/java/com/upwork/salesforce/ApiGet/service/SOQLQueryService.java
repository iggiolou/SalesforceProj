package com.upwork.salesforce.ApiGet.service;

import static com.upwork.salesforce.ApiGet.config.Constants.INSTANCE_URL;
import static com.upwork.salesforce.ApiGet.config.Constants.URL_QUERY;
import static org.springframework.security.oauth2.common.OAuth2AccessToken.ACCESS_TOKEN;
import static org.springframework.security.oauth2.common.OAuth2AccessToken.TOKEN_TYPE;

import com.upwork.salesforce.ApiGet.controller.Authentication;
import java.util.Map;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author viktoriyadoroshenko
 * @since 7/6/18
 */
@Service
public class SOQLQueryService {

  RestTemplate restTemplate = new RestTemplate();

  public String runSelectQuery(String query) throws ParseException {

    Map authParams = new Authentication().getAuthParams();
    String url = authParams.get(INSTANCE_URL) + URL_QUERY + query;

    ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET,
        createHttpEntity(
            authParams.get(TOKEN_TYPE).toString() + " " + authParams.get(ACCESS_TOKEN)),
        String.class);
    System.out.println(response.getBody());
    return response.getBody();

  }


  private HttpEntity createHttpEntity(String token) {
    HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.AUTHORIZATION, token);
    MediaType jsonMimeType = MediaType.APPLICATION_JSON;
    headers.setContentType(jsonMimeType);
    return new HttpEntity(headers);

  }

}
